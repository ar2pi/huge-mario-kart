const os = require('os');
const chalk = require('chalk');
const gpio = require('rpi-gpio');

const _ = (color, str) => chalk[color](str);
const log = (...stuff) => console.log(_('magenta', stuff[0]), ...stuff.slice(1));
const info = (...stuff) => console.log(_('green', stuff[0]), ...stuff.slice(1));
const error = (...stuff) => console.log(_('red', stuff[0]), ...stuff.slice(1));

const io = require('socket.io')({
    transports: ['websocket']
});

const gpiop = gpio.promise;

const LOCAL_IP = os.networkInterfaces().wlan0[0].address;
const PIN = 7;

const socketListen = () => {
    io.on('connection', function(socket) {
        log('Socket connected');
        socket.on('player:connected', function() {
            log('player:connected');
            gpiop.write(PIN, true);
        });
        socket.on('player:input', function(payload) {
            log('player:input', payload);
            gpiop.write(PIN, payload.data === 'on');
        });
    });
};

const blink = () => {
    setInterval(() => {
        gpiop.write(PIN, true, () => {
            setTimeout(() => {
                gpiop.write(PIN, false);
            }, 300);
        });
    }, 1000);
};

gpiop
    .setup(PIN, gpio.DIR_OUT)
    .then(() => {
        info(`GPIO interface ready, pin ${PIN}`);
        io.attach(4567);
        socketListen();
        info(`Web Socket available on ws:\/\/${LOCAL_IP}:4567`);
        setTimeout(blink, 300);
    })
    .catch((err) => {
        error(`Error: ${err.toString()}`);
    });
